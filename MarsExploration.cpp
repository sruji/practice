#include <iostream>
#include <string>

using namespace std;

int main() {
  string S;
  int ctr = 0,len;
  cin >> S;
  len = S.size();
  for ( int i = 0; i < len; i += 3 ) {
    if ( S[i] != 'S' )  ctr++;
    if ( S[i+1] != 'O') ctr++;
    if ( S[i+2] != 'S') ctr++;
  }
  cout << ctr;
  return 0;
}
