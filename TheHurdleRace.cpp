#include <iostream>
#include <vector>

using namespace std;

int main() {
  int n,k,h,ans;
  vector<int>v;
  cin >> n >> k;
  for ( int i = 0; i < n; i++ ) {
    cin >> h;
    v.push_back(h);
  }
  sort(v.begin(),v.end());
  ans = v[v.size()-1] - k;
  if ( ans < 0 ) {
    cout << "0";
  }
  else {
    cout << ans;
  }
  return 0;
}
