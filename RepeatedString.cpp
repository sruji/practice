#include <iostream>
#include <string>

using namespace std;

int main() {
  string s;
  cin >> s;
  long n,i,temp1,temp2,ctr=0,ans;
  cin >> n;
  temp1 = (n/s.size());
  temp2 = (n%s.size());
  for ( i = 0; i < s.size(); i++ ) {
    if (s[i] == 'a') ctr++;
  }
  ans = temp1 * ctr;
  ctr = 0;
  for ( i = 0; i < temp2; i++ ) {
    if ( s[i] == 'a' ) ctr++;
  }
  ans += ctr;
  cout << ans << endl;
  return 0;
}
