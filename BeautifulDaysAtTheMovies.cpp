#include <iostream>

using namespace std;

int rev( int x ) {
  int a = 0;
  while ( x > 0 ) {
    a = a*10 + x%10;
    x = x/10;
  }
  return a;
}

int main() {
  int i,j,k,ans = 0,ctr;
  cin >> i >> j >> k;
  for ( ctr = i; ctr <= j; ctr++ ) {
    if ( ((ctr - rev(ctr)) % k )== 0) {
      ans++;
    }
  }
  cout << ans << endl;
  return 0;
}
