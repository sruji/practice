#include <iostream>
#include <vector>
#include <cstdlib>
#include <algorithm>

using namespace std;

int main(){
    int n,max = 0,ctr;
    cin >> n;
    vector<int> a(n);
    for(int a_i = 0;a_i < n;a_i++){
       cin >> a[a_i];
    }
    sort(a.begin(),a.end());
    for(int i = 0; i < n - 1; i++){
      ctr = 0;
      for( int j = i+1; j < n; j++ ) {
        if( abs(a[j]-a[i]) <= 1 ) {
          ctr++;
        }
      }
      if ( ctr > max )  max = ctr;
    }
    cout << max+1 << endl;
    return 0;
}
