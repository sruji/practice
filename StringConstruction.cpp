#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main() {
  int q;
  cin >> q;
  while ( q-- > 0 ) {
    string s;
    cin >> s;
    int ctr = 0;
    vector<int> arr(26,0);
    for ( int i = 0; i < s.length(); i++ ) {
      ++arr[s[i]-'a'];
    }
    for ( int j = 0; j < 26; j++) {
      if ( arr[j] != 0 ) {
        ctr++;
      }
    }
    cout << ctr << endl;
  }
  return 0;
}
