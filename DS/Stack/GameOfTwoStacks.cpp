#include <bits/stdc++.h>

using namespace std;

int arr1[100005];
int arr2[100005];

int main() {
  int g;
  cin >> g;
  while ( g-- ) {
    int n,m,x,i;
    cin >> n >> m >> x;
    for ( i = 0; i < n; i++ ) cin >> arr1[i];
    for ( i = 0; i < m; i++ ) cin >> arr2[i];
    int score = 0, d1 = 0, d2 = 0, sum = 0;
    while ( d1 < n && sum + arr1[d1] <= x) {
      sum += arr1[d1++];
    }
    score = d1;
    while ( d2 < m && d1 >= 0 ) {
      sum += arr2[d2++];
      while ( sum > x && d1 > 0 ) {
        sum -= arr1[--d1];
      }
      if ( sum <= x && (score < d1 + d2 ) ) {
         score = d1 + d2;
      }
    }
    cout << score << endl;
  }
  return 0;
}
