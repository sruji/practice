#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>


struct node {
    char str[50];
    struct node* next;
};

struct node* start = NULL;

void create_ll() {
    struct node *ptr,*p;
    ptr = (struct node*)malloc(sizeof(struct node));
    char s[25];
    scanf("%s",s);
    strcpy(ptr->str,s);
    if ( start == NULL ) {
      start = ptr;
      ptr->next = NULL;
    }
    else {
      p = start;
      while ( p->next != NULL )
        p = p->next;
      p->next = ptr;
      ptr->next = NULL;
    }
}

int count() {
    struct node *p;
    int ctr = 0;
    char s[25];
    scanf("%s",s);
    p = start;
    while ( p->next != NULL ) {
      if(strcmp(p->str,s) == 0) {
        ctr++;
      }
      p = p->next;
    }
    if(strcmp(p->str,s) == 0) {
        ctr++;
     }
    return ctr;
}

int main() {
    int N,Q,x;
    scanf("%d",&N);
    while ( N-- ) {
        create_ll();
    }
    scanf("%d",&Q);
    while( Q-- ) {
       x = count();
       printf("%d\n",x);
    }
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    return 0;
}
