
struct Node {
  int data;
  struct Node *next;
}

struct Node *headA = NULL, *headB = NULL;

int CompareLists(Node *headA, Node* headB) {
  int ans = 0,ctr1=1,ctr2=1;
  struct Node *ptr1,*ptr2;
  if ( headA == NULL || headB == NULL) return 0;
  else {
    ptr1 = headA;
    ptr2 = headB;
    while ( ptr1->next != NULL ) {  ptr1 = ptr1->next; ctr1++;}
    while ( ptr2->next != NULL ) {  ptr2 = ptr2->next; ctr2++;}
    if ( ctr1 == ctr2 ) {
      ptr1 = headA;
      ptr2 = headB;
      while ( ptr1->next != NULL ) {
        if ( ptr1->data == ptr2->data ) {
          ans = 1;
          ptr1 = ptr1->next;
          ptr2 = ptr2->next;
        }
        else { ans = 0; break;}
      }
      if ( ptr1->data != ptr2->data ) ans = 0;
    }
  }
  return ans;
}
