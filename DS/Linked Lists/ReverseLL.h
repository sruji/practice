
struct Node {
  int data;
  struct Node *next;
};

struct Node *head = NULL;

Node* Reverse(Node *head) {
  struct Node *ptr;
  if ( head == NULL ) { return NULL; } // List doesn't exist
  if ( head->next == NULL ) { return head; } // list with single node
  ptr = Reverse(head->next); // recursive call on rest of the List
  head->next->next = head; // make first, link to the last node in the reversed rest.
  head->next = NULL; // since first is the new last
  return ptr;
}
