
struct Node
{
   int data;
   struct Node *next;
};

struct Node *head = NULL;

Node* Delete(Node *head, int position) {
  struct Node *temp,*pre;
  if ( position == 0 ) {
    temp = head;
    head = head->next;
    free(temp);
  }
  else {
    temp = head;
    for ( int i = 0; i < position; i++) {
      pre = temp;
      temp = temp->next;
    }
    pre->next = temp->next;
    free(temp);
  }
  return head;
}
