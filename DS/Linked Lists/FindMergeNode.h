struct Node {
  int data;
  struct Node *next;
};

Node *head - NULL;

int FindMergeNode(Node *headA, Node *headB) {
  Node *ptr1, *ptr2;
  ptr1 = headA;
  ptr2 = headB;
  if ( ptr1->next != NULL && ptr2->next != NULL ) {
    if ( ptr2->next->data != ptr1->next->data ) ptr1 = ptr1->next;
    if ( ptr1->next->data != ptr2->next->data ) ptr2 = ptr2->next;
  }
  return ptr1->next->data;
}
