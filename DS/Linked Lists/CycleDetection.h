struct Node {
  int data;
  struct Node *next;
};

struct Node* head = NULL;

bool has_cycle(Node *head) {
  if(head == NULL) {
    return false;
  }
  struct Node* slow = head;
  struct Node* fast = head;

  while( fast != NULL && fast->next != NULL ) {
    slow = slow->next;
    fast = fast->next->next;
    if( slow == fast ) return true;
  }
  return false;
}
