
struct Node {
  int data;
  struct Node *next;
};

struct Node *head = NULL;

Node *Insert(Node *head, int data) {
  struct Node *ptr,*p;
  ptr = new Node;
  ptr->data = data;
  if ( head == NULL ) {
    head = ptr;
    ptr->next = NULL;
  }
  else {
    p = head;
    while ( p->next != NULL ) {
      p = p->next;
    }
    p->next = ptr;
    ptr->next = NULL;
  }
  return head;
}
