struct Node {
  int data;
  Node *next;
  Node *prev;
}head;

head = NULL;

Node* Reverse(Node* head) {
  Node *temp, *ptr;
  ptr = head;
  if ( head == NULL ) return NULL;
  else {
    while ( ptr->next != NULL ) {
      temp = ptr;
      ptr = ptr->next;
    }
    head = ptr;
    while ( ptr != NULL ) {
      ptr->prev = ptr->next;
      ptr->next = temp;
      if ( temp != NULL ) temp = temp->prev;
      ptr = ptr->next;
    }
  }
  return head;
}
