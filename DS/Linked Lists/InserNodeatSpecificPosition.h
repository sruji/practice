
struct Node {
  int data;
  struct Node *next;
};

struct Node *head = NULL;

Node *InsertNth(Node *head, int data, int position) {
  struct Node *ptr,*p;
  ptr = new Node;
  ptr->data = data;
  if ( position == 0 ) {
    ptr->next = head;
    head = ptr;
  }
  else {
    p = head;
    for ( int i = 0; i < position-1; i++ ) {
      p = p->next;
    }
    ptr->next = p->next;
    p->next = ptr;
  }
  return head;
}
