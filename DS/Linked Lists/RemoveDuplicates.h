struct Node {
  int data;
  struct Node *next;
};

struct Node *head = NULL;

Node* RemoveDuplicates(Node *head) {
  Node *ptr = new Node;
  ptr = head;
  if ( head == NULL ) return NULL;
  else {
    while ( ptr->next != NULL ) {
      Node *temp = new Node;
      temp = ptr;
      if ( ptr->data == ptr->next->data ) {
        temp = ptr->next;
        ptr->next = temp->next;
        delete temp;
        }
      else ptr = ptr->next;
      }
  }
  return head;
}
