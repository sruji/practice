
struct Node {
  int data;
  struct Node *next;
};

struct Node *head = NULL;

Node* Insert(Node *head,int data) {
  struct Node *ptr;
  ptr = new Node;
  ptr->data = data;
  if ( head == NULL ) {
    head = ptr;
    ptr->next = NULL;
  }
  else {
    ptr->next = head;
    head = ptr;
  }
  return head;
}
