struct Node {
  int data;
  struct Node *next;
};

struct Node *head = NULL;

int GetNode(Node *head,int positionFromTail) {
  int len = 0;
  struct Node *ptr = head;
  while ( ptr != NULL ) {
    len++;
    ptr = ptr->next;
  }
  ptr = head;
  for ( int i = 0; i < len - positionFromTail - 1; i++ ) {
    ptr = ptr->next;
  }
  return ptr->data;
}
