struct Node {
  int data;
  Node *next;
  Node *prev;
}head;

head = NULL;

Node* makeNode(int data){
    Node* newNode = (Node*) malloc(sizeof(Node));
    newNode->data = data;
    newNode->next = 0;
    newNode->prev = 0;
    return newNode;
}

Node* SortedInsert(Node *head,int data){
    if (!head) {
        Node* newNode = makeNode(data);
        return newNode;
    }
    if (data < head->data){
        Node* newNode = makeNode(data);
        newNode->next = head;
        head->prev = newNode;
        return newNode;
    }
    else{
        head->next = SortedInsert(head->next, data);
        head->next->prev = head;
        return head;
    }
}
