
int height(Node* root) {
    if ( root == NULL ) return 0;     // Base Case
    queue<Node *>q;              //  For Maintaining Level Order
    q.push(root);               // Initialising
    int height = 0;
    while (1) {
      int NodeCount = q.size();                 // Nodes at current level
      if ( NodeCount == 0 ) return height;
      height++;
      while ( NodeCount > 0 ) {               // Leave Current Level and add next Level
        Node *node = q.front();
        q.pop();
        if ( node->left != NULL ) q.push(node->left);
        if ( node->right != NULL ) q.push(node->right);
        NodeCount--;
      }
    }
}

// Solution 2

int height(Node *root) {
  if ( root == NULL ) return -1;
  else {
    return 1 + max(height(root->left),height(root->right));
  }
}
