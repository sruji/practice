struct Node {
  int data;
  Node *left;
  Node *right;
}root;

void preOrder(node *root) {
  if ( root != NULL ) {
    cout << root->data << " ";
    preOrder(root->left);
    preOrder(root->right);
  }
}
