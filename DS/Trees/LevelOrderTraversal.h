
int height(node* root) {
  if ( root == NULL ) return 0;
  else {
    int lheight = height(root->left);
    int rheight = height(root->right);
    if ( lheight > rheight ) return (lheight+1);
    else return (rheight+1);
  }
}

void printGivenLevel(node *root, int level) {
  if ( root == NULL ) return;
  if ( level == 1 ) cout << root->data << " ";
  else if ( level > 1 ) {
    printGivenLevel(root->left, level-1);
    printGivenLevel(root->right, level-1);
  }
}

void levelOrder(node * root) {
    int h = height(root);
    int i;
    for( i = 1; i <=h; i++ ) printGivenLevel(root,i);
}
