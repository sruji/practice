
node *newNode( int item ) {
  node *temp = new node;
  temp->data = item;
  temp->left = NULL;
  temp->right = NULL;
  return temp;
}

node * insert(node * root, int value) {
  if ( root == NULL ) root = newNode(value);
  if ( value < root->data )
    root = insert(root->left, value);
  else {
    root = insert(root->right, value);
  }
  return root;
}
