#include <iostream>
#include <vector>

using namespace std;

int main(){
    int n,diff;
    cin >> n;
    vector<int> A(n);
    vector<int> D;
    for(int i = 0;i < n;i++){
       cin >> A[i];
    }
    for ( int i = 0; i < n - 1; i++ ) {
      for ( int j = i + 1; j < n; j++ ) {
        if ( A[i] == A[j] ) {
          diff = abs(i - j);
          D.push_back(diff);
        }
      }
    }
    if ( D.size() == 0 ) {
     cout << "-1" << endl;
    }
    else {
      sort(D.begin(), D.end());
      cout << D[0] << endl;
    }
    return 0;
}
