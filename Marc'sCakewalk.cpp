#include <bits/stdc++.h>

using namespace std;

long Exp(long n)
{
       long res=1,x=2;
       for (int i=1;i<=n;i++)
          res = res*x;
       return res;
}

int main(){
    int n,ctr = 0;
    long miles;
    cin >> n;
    vector<int> calories(n);
    for(int calories_i = 0; calories_i < n; calories_i++){
       cin >> calories[calories_i];
    }
    sort(calories.begin(),calories.end());
    for ( int i = calories.size() - 1; i >= 0; i-- ) {
      miles += (Exp(ctr)*calories[i]);
      ctr++;
    }
    cout << miles;
    // your code goes here
    return 0;
}
