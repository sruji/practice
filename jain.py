# cook your dish here
T = int(input())

while T > 0:
    N = int(input())
    l = [0 for x in range(32)]
    ans = 0
    weights = {'a':0,'e':1,'i':2,'o':3,'u':4}
    for i in range(N):
        temp = [0 for t in range(5)]
        s = input()
        idx = 0
        for ch in s:
            temp[weights[ch]] = 1
        #print(idx)
        for x in range(len(temp)):
            idx += (temp[x] * (2**x))
        l[idx] += 1
    #print(l)
    for i in range(32):
        for j in range(32):
            if i != j:
                if i | j == 31 and i < j:
                    ans += l[i] * l[j]

    n = l[31]
    ans += ((n*(n-1))//2)
    print(ans)
    T -= 1
