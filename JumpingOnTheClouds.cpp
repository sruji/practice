#include <iostream>
#include <vector>

using namespace std;

int main() {
    int n,ctr=0,pos=0;
    cin >> n;
    vector<int> c(n);
    for(int i = 0; i < n; i++){
       cin >> c[i];
    }
    while ( pos < n - 1 ) {
      if ( c[pos + 2] == 0 && (pos + 2) < n ) {
        pos += 2;
      }
      else {
        pos++;
      }
      ctr++;
    }
    cout << ctr << endl;
    return 0;
}
