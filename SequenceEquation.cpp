#include <iostream>
#include <vector>

using namespace std;

int search(int a, vector<int>& V) {
  int k,i;
  for( i = 0; i < V.size(); i++ ) {
    if ( a == V[i] ) {
      k = i+1;
      break;
    }
  }
  return k;
}


int main() {
  int n,t,x,y;
  cin >> n;
  vector<int> v;
  for ( int i = 0; i < n; i++ ) {
    cin >> t;
    v.push_back(t);
  }
  for ( int i = 0; i < n; i++ ) {
    x = search(i+1,v);
    y = search(x,v);
    cout << y << endl;
  }
  return 0;
}
