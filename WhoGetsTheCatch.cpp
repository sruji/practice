#include <bits/stdc++.h>

using namespace std;

int whoGetsTheCatch(int n, int x, vector < int > X, vector < int > V){
    vector<int> T(n);
    int pos,min;
    for ( int T_i = 0; T_i < n; T_i++ ) {
      T[T_i] = X[T_i]/V[T_i];
    }
    min = T_i[0];
    pos = 0;
    for ( int i = 1; i < n; i++ ) {
      for ( int j = 0; j < i; j++ ) {
        if ( T[T_i] == T[T_j]) {
          return -1;
        }
      }
    }
    for ( int i = 1; i < n; i++ ) {
      if ( T[i] < min ) {
        min = T[i];
        pos = i;
      }
    }
    return pos;
}

int main() {
    //  Return the index of the catcher who gets the catch, or -1 if no one gets the catch.
    int n;
    int x;
    cin >> n >> x;
    vector<int> X(n);
    for(int X_i = 0; X_i < n; X_i++){
       cin >> X[X_i];
    }
    vector<int> V(n);
    for(int V_i = 0; V_i < n; V_i++){
       cin >> V[V_i];
    }
    int result = whoGetsTheCatch(n, x, X, V);
    cout << result << endl;
    return 0;
}
