#include <bits/stdc++.h>

using namespace std;

int main(){
    int n,diff,min;
    cin >> n;
    vector<int> a(n);
    vector<int> v;
    for(int a_i = 0; a_i < n; a_i++){
       cin >> a[a_i];
    }
    sort(a.begin(),a.end());
    diff = abs(a[0]-a[1]);
    for ( int i = 1; i < n - 1; i++ ) {
      if ( abs(a[i]-a[i+1]) < diff )
        diff = abs(a[i] - a[i+1]);
    }
    cout << diff;
    return 0;
}
