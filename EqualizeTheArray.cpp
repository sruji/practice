#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
  int arr[105],n,ctr,temp,i,ans;
  vector<int>v;
  cin >> n;
  for ( i = 0; i < n; i++ ) {
    cin >> arr[i];
  }
  sort(arr, arr + n);
  temp = arr[0];
  ctr = 1;
  for ( i = 1; i < n; i++ ) {
    if ( arr[i] == temp ) {
      ctr++;
    }
    else {
      v.push_back(ctr);
      temp = arr[i];
      ctr = 1;
    }
  }
  v.push_back(ctr);
  sort(v.begin(),v.end());
  ans = n - v[v.size() - 1];
  cout << ans;
  return 0;
}
