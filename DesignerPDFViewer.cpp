#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main(){
    vector<int> h(26);
    vector<int> v;
    int area,x;
    for(int h_i = 0; h_i < 26; h_i++){
       cin >> h[h_i];
    }
    string word;
    cin >> word;
    for ( int i = 0; i < word.size(); i++ ) {
      x = word[i] - 97;
      v.push_back(h[x]);
    }
    sort(v.begin(),v.end());
    area = word.size()*v[v.size()-1];
    cout << area;
    return 0;
}
