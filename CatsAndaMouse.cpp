#include <iostream>
#include <math>

using namespace std;

int main() {
  int T,a,b,c,d1,d2;
  cin >> T;
  while ( T-- ) {
    cin >> a >> b >> c;
    d1 = abs(a-c);
    d2 = abs(b-c);
    if ( d1 > d2 ) {
      cout << "Cat B" << endl;
    }
    else if ( d1 == d2 ) {
      cout << "Mouse C" << endl;
    }
    else {
      cout << "Cat A" << endl;
    }
  }
  return 0;
}
