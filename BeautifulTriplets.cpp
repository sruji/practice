#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int x,n,d,ctr = 0;
    cin >> n >> d;
    vector<int> v;
    for ( int i = 0; i < n; i++ ) {
        cin >> x;
        v.push_back(x);
    }
    for ( int i = 0; i < n - 2; i++ ) {
      for ( int j = 1; j < n - 1; j++ ) {
        if ( v[j] - v[i] == d ) {
          for ( int k = j + 1; k < n; k++ ) {
            if ( v[k] - v[j] == d ) ctr++;
          }
        }
      }
    }
    cout << ctr;
    return 0;
}
