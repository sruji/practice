#include <iostream>
#include <vector>
typedef long long ll;

using namespace std;

 int main() {
    ll T;
    cin >> T;
    while(T--) {
        vector<ll> C;
        vector<ll> L;
        ll N,c,l,temp;
        cin >> N;
        for(int i=0; i < N; i++) {
            cin >> c;
            C.push_back(c);
        }
        temp = C[0];
        for(int i=1; i < N; i++) {
            temp = min(C[i],temp);
            C[i] = temp;
        }
        ll cost = 0;
        for(int i=0; i < N; i++) {
            cin >> l;
            L.push_back(l);
        }
        for(int i=0; i < N; i++) {
            cost += (C[i]*L[i]);
        }
        cout << cost << endl;
    }
}
