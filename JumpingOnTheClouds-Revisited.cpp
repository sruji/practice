#include <iostream>
#include <vector>

using namespace std;

int main(){
    int n;
    int k;
    int E = 100;
    int pos = 0;
    cin >> n >> k;
    vector<int> c(n);
    for(int c_i = 0;c_i < n;c_i++){
       cin >> c[c_i];
    }
    while (1) {
      E -= 1;
      if ( c[pos] == 1 ) {
        E -= 2;
      }
      pos = (pos + k) % n;
      if ( pos == 0 ) {
          break;
      }
    }
    cout << E << endl;
    return 0;
}
